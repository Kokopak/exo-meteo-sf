<?php

namespace App\Service;

use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\HttpClient;

class WeatherService
{
    private $client;
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->client = HttpClient::create();
        $this->apiKey = $apiKey;
    }

    public function getWeather($lat, $long)
    {
        $response = $this->client->request('GET', 'https://api.darksky.net/forecast/' . $this->apiKey . '/' . $lat . ',' . $long, [
                'query' => [
                    'lang' => 'fr',
                    'units' => 'si'
                ]
            ]

        );
        try {
            $response = $response->toArray();
            return $response;
        } catch (ClientException $e) {
            return array("message" => "Erreur lors de l'appel de l'API");
        }

    }

    public function getCityCoordinates($city)
    {
        $response = $this->client->request('GET', 'https://api-adresse.data.gouv.fr/search/?q=' . $city->getName());

        try {
            $response = $response->toArray();
            if ($response['features']) {
                $firstCity = $response['features'][0];
                $cityCoordinates = $firstCity['geometry']['coordinates'];

                $city->setName($firstCity['properties']['name']);
                $city->setLat($cityCoordinates[1]);
                $city->setLong($cityCoordinates[0]);
                $city->setPostcode($firstCity['properties']['postcode']);
            }
        } catch (ClientException $e) {
        }

    }
}
