<?php

namespace App\Controller;

use App\Entity\City;
use App\Service\WeatherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class WeatherController extends AbstractController
{
    private $wheatherService;

    public function __construct(WeatherService $weather)
    {
        $this->weatherService = $weather;
    }

    public function index(Request $request)
    {
        $city = new City();
        $city->setName("Toulouse");

        $currentWeather = array("currently" => null, "daily" => array("data" => null));

        $form = $this->createFormBuilder($city)
            ->add('name', TextType::class)
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                $city = $form->getData();
            }
        }

        $this->weatherService->getCityCoordinates($city);
        if ($city->getLat()) {
            $currentWeather = $this->weatherService->getWeather($city->getLat(), $city->getLong());
        }

        return $this->render('weather/index.html.twig', array(
            'current' => $currentWeather['currently'],
            'daily_weather' => $currentWeather['daily']['data'],
            'city' => $city,
            'form' => $form->createView()
        ));
    }

    public function setCity(Request $request)
    {


    }
}
