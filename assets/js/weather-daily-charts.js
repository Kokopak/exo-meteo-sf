document.addEventListener('DOMContentLoaded', function () {
    var chart = document.getElementById('myChart');

    var dailyWeather = JSON.parse(chart.dataset.dailyWeather);
    var dailyWeatherHigh = dailyWeather.map((dailyData) => dailyData.temperatureHigh);
    var dailyWeatherLow = dailyWeather.map((dailyData) => dailyData.temperatureLow);

    var dailyWeatherDays = dailyWeather.map((dailyData) => {
        var d = new Date(0);
        d.setUTCSeconds(dailyData.time);

        var dd = d.getDate();
        var mm = d.getMonth() + 1;
        var yyyy = d.getFullYear();

        dd = dd < 10 ? '0' + dd : dd;
        mm = mm < 10 ? '0' + mm : mm;

        return dd + '/' + mm + '/' + yyyy;
    });

    var ctx = chart.getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dailyWeatherDays,
            datasets: [
                {
                    label: 'Température plus haute',
                    data: dailyWeatherHigh,
                    backgroundColor: '#ff6384',
                    borderColor: '#ff6384',
                    fill: false
                },
                {
                    label: 'Température plus basse',
                    data: dailyWeatherLow,
                    backgroundColor: '#36a2eb',
                    borderColor: '#36a2eb',
                    fill: false
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
});
