document.addEventListener('DOMContentLoaded', function () {
    var elem = document.querySelectorAll('.autocomplete')[0];
    var controller = new AbortController();
    var instances = M.Autocomplete.init(elem, {
        data: {}
    });

    elem.addEventListener('input', (evt => {
        var newData = {};
        controller.abort();
        fetch('https://api-adresse.data.gouv.fr/search/?q=' + elem.value)
            .then(response => response.json())
            .then(data => {
                data.features.forEach((feature) => {
                    newData[feature.properties.label] = null
                });
                instances.updateData(newData);
            });
    }));
});
